class Caster {
  private int[] point = new int[2];
  private PVector position;
  private int lines;
  private int strokeWeight;

  public Caster(int[] point, int l, int sw) {
    this.point = point;
    position = g.getPointVector(point[0], point[1]);

    strokeWeight = sw;

    if (l < 1) {
      println("Caster lines number must be positive and greater than 0");
      lines = 1;
    } else {
      lines = l;
    }
  }

  public Caster(int[] point, int l) {
    this(point, l, 1);
  }

  public PVector[] cast(int x, int y, int DIRECTION, float amt) {
    PVector points[] = new PVector[1 + lines * 2];

    pushStyle();
    strokeWeight(strokeWeight);

    for (int i = 0; i < lines; i++) {
      if (i == 0) {
        PVector t = g.getLerpedVector(position, g.getPointVector(x, y), amt);
        line(position.x, position.y, t.x, t.y);
        points[i] = t.copy();
      } else {
        PVector tPlus;
        PVector tMinus;
        if (DIRECTION == HORIZONTAL) {
          tPlus = g.getLerpedVector(position, g.getPointVector(x + i, y), amt);
          tMinus = g.getLerpedVector(position, g.getPointVector(x - i, y), amt);
        } else if (DIRECTION == VERTICAL) {
          tPlus = g.getLerpedVector(position, g.getPointVector(x, y + i), amt);
          tMinus = g.getLerpedVector(position, g.getPointVector(x, y - i), amt);
        } else {
          println("Caster.cast() DIRECTION must be HORIZONTAL or VERTICAL");
          break;
        }
        points[i * 2] = tPlus.copy();
        points[i * 2 - 1] = tMinus.copy();

        line(position.x, position.y, tPlus.x, tPlus.y);
        line(position.x, position.y, tMinus.x, tMinus.y);
      }
    }

    popStyle();

    return points;
  }

  public PVector[] cast(int x, int y, int DIRECTION) {
    return cast(x, y, DIRECTION, 1);
  }

  public void castDivided(int x, int y, int DIRECTION, float amt) {
    PVector[] half = new PVector[0];

    // Draw first half until 0.5
    if (amt < 0.5) {
      if (DIRECTION == HORIZONTAL) {
        half = cast(x / 2, y / 2, DIRECTION, amt * 2);
      } else if (DIRECTION == VERTICAL) {
        half = cast(x / 2, y / 2, DIRECTION, amt * 2);
      }
    } else {
      // keep drawing first half but also draw second half
      if (DIRECTION == HORIZONTAL) {
        half = cast(x / 2, y / 2, DIRECTION);
      } else if (DIRECTION == VERTICAL) {
        half = cast(x / 2, y / 2, DIRECTION);
      }

      pushStyle();
      strokeWeight(strokeWeight);

      // Draw second half towards target
      PVector target = g.getLerpedVector(position, g.getPointVector(x, y), amt);
      for (int i = 0; i < lines * 2 - 1; i++) {
        line(half[i].x, half[i].y, target.x, target.y);
      }
      popStyle();
    }
  }

  public void castDivided(int x, int y, int DIRECTION) {
    castDivided(x, y, DIRECTION, 1);
  }
  
  public void castArround(float amt) {
    pushStyle();
    strokeWeight(strokeWeight);
    for(int i = -lines; i <= lines; i++) {
      //Bottom left
      PVector blFrom = g.getPointVector(point[0] - lines, point[1] + i);
      PVector blTo = g.getLerpedVector(blFrom, g.getPointVector(point[0] + i, point[1] + lines), amt);
      line(blFrom.x, blFrom.y, blTo.x, blTo.y);
      
      ////Top Right
      PVector trFrom = g.getPointVector(point[0] + lines, point[1] + i);
      PVector trTo = g.getLerpedVector(trFrom, g.getPointVector(point[0] + i, point[1] - lines), amt);
      line(trFrom.x, trFrom.y, trTo.x, trTo.y);
    }
    
    popStyle();
  }
  
  public void castArround() {
    castArround(1);
  }

  public int getLines() {
    return lines;
  }

  public PVector getPosition() {
    return position;
  }
  
  public int[] getPoint() {
    return point;
  }
}
