class SecondLayer extends Layer {
  private Caster[] casters;
  private PVector[] targets;
  private int direction;

  public SecondLayer(int DIRECTION) {
    super();
    casters = new Caster[2];
    targets = new PVector[2];
    direction = DIRECTION;

    int variation = round(random(0,15));
    int lines = halfCells + 1 + variation;

    if (direction == HORIZONTAL) {
      casters[0] = new Caster(g.getPoint(halfCells + variation, 0), lines);
      casters[1] = new Caster(g.getPoint(-halfCells - variation, 0), lines);
      targets[0] = new PVector(0, quarterCells + variation);
      targets[1] = new PVector(0, -quarterCells - variation);
    } else if (direction == VERTICAL) {
      casters[0] = new Caster(g.getPoint(0, halfCells + variation), lines);
      casters[1] = new Caster(g.getPoint(0, -halfCells - variation), lines);
      targets[0] = new PVector(quarterCells + variation, 0);
      targets[1] = new PVector(-quarterCells - variation, 0);
    }
  }

  public void render() {
    pushMatrix();
    pushStyle();
    translate(width / 2, height / 2);
    rotate(rotation);

    stroke(colour);

    casters[0].cast(int(targets[0].x), int(targets[0].y), direction, progress);
    casters[0].cast(int(targets[1].x), int(targets[1].y), direction, progress);
    casters[1].cast(int(targets[0].x), int(targets[0].y), direction, progress);
    casters[1].cast(int(targets[1].x), int(targets[1].y), direction, progress);

    popStyle();
    popMatrix();
  }
  
  public Caster[] getCasters() {
    return casters; 
  }
  
  public PVector[] getTargets() {
   return targets; 
  }
}
