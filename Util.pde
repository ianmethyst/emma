boolean throwCoin() {
  int coin = int(random(0, 2));

  if (coin == 0) {
    return false;
  } else {
    return true;
  }
}

float getClosestSlideTarget(float angle) {
  float closest = 1000;
  
  for (int i = 0; i < slideTargets.length; i++) {
    if (closest == -1 || abs(angle - slideTargets[i]) < abs(closest - angle)) {
      closest = slideTargets[i];
    }
  }
  
  return closest;
}

void drawAxis() {
  pushStyle();
  stroke(0, 100, 100);
  line(width / 2, 0, width /2, height);
  line(0, height / 2, width, height / 2);
  popStyle();
}
