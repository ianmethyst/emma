class Cursor {
  PVector l, r;
  PVector lT, rT;

  boolean ease;
  final float easing = 0.05;

  float rLastActivity, lLastActivity;
  boolean rActive, lActive;

  int size = 30; // Size for debugging;

  public Cursor(boolean e) {
    rLastActivity = lLastActivity = -1;
    r = new PVector(width - size * 2, height / 2);
    l = new PVector(size * 2, height / 2);
    rT = r.copy();
    lT = l.copy();

    ease = e;
  }

  public void render() {
    pushStyle();

    fill(0, 0, 100, 200);
    noStroke();

    if (lActive) {
      fill(0, 50, 100, 225);
    }

    pushMatrix();
    translate(l.x, l.y);
    ellipse(0, 0, size, size); 
    popMatrix();

    if (rActive) {
      fill(180, 50, 100, 225);
    }

    pushMatrix();
    translate(r.x, r.y);
    ellipse(0, 0, size, size);
    popMatrix();

    popStyle();
  }

  public void ease() {
    PVector lD = PVector.sub(lT, l);
    lD.mult(easing);
    l.add(lD);

    PVector rD = PVector.sub(rT, r);
    rD.mult(easing);
    r.add(rD);
  }

  public void update() {
    int m = millis();

    if (ease == true) {
      ease();
    }

    if (m < rLastActivity + 1 * 1000) {
      rActive = true;
    } else {
      rActive = false;
    }

    if (m < lLastActivity + 1 * 1000) {
      lActive = true;
    } else {
      lActive = false;
    }

    println(getActive());

    if (getActive()) {
      p.setLifespan();
    }
  }

  public void debug() {
    setLeft(mouseX, mouseY);
    setRight(width - mouseX, height - mouseY);
  }

  public boolean getActive() {
    return lActive && rActive;
  }


  // This method is used to check if posenet couldn't determine the position of two hands
  private boolean checkDist(PVector actual, PVector data) {
    if (PVector.dist(actual, data) > 80) {
      return true;
    } else {
      return false;
    }
  }

  private PVector mapPVector(float x, float y) {
    // TODO
    float mappedX = map(x, 0, HEIGHT, 0, HEIGHT);  
    float mappedY = map(y, 0, WIDTH, 0, WIDTH);

    return new PVector(mappedX, mappedY);
  }

  public void setLeft(float x, float y) {
    PVector data = mapPVector(x, y);
    if (checkDist(r, data)) {
      if (ease) {
        lT.set(data);
      } else {
        l.set(data);
      }
      lLastActivity = millis();
    }
  }

  public void setRight(float x, float y) {
    PVector data = mapPVector(x, y);
    if (checkDist(l, data)) {
      if (ease) {
        rT.set(data);
      } else {
        r.set(data);
      }
      rLastActivity = millis();
    }
  }

  // Return distance relative to window size (from 0 to 1)
  public float getDist() {
    return map(PVector.dist(l, r), 0, dist(0, 0, width, height), 0, 1);
  }

  public float getAngle() {
    return atan2(l.y - r.y, l.x - r.x);
  }
}
