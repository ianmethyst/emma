class Grid {
  private int cells;
  private PVector points[][];

  private color gridColor;

  public Grid(int cells) {
    this.cells = cells;
    points = new PVector[cells][cells];
    println(cells);

    gridColor = color(0, 0, 90);

    for (int x = 0; x < cells; x++) {
      for (int y = 0; y < cells; y++) {
        points[x][y] = new PVector(map(x, 0, cells - 1, - WIDTH / 2, WIDTH / 2), map(y, 0, cells - 1, -HEIGHT / 2, HEIGHT / 2));
      }
    }
  }

  public void render() {
    pushStyle();
    pushMatrix();

    translate(width / 2, height / 2);
    stroke(gridColor);

    for (int x = 0; x < cells; x++) {
      line(points[x][0].x, points[x][0].y, points[x][cells - 1].x, points[x][cells - 1].y);
    }

    for (int y = 0; y < cells; y++) {
      line(points[0][y].x, points[0][y].y, points[cells - 1][y].x, points[cells - 1][y].y);
    }

    popStyle();
    popMatrix();
  }

  public int getCells() {
    return cells;
  }
  
  public int[] getPoint(int x, int y) {
    int[] point = new int[2];
    point[0] = x;
    point[1] = y;
    return point;
  }

  public PVector getPointVector(int x, int y) {
    int xMapped = round(map(x, -cells / 2, cells / 2, 0, cells - 1));
    int yMapped = round(map(y, -cells / 2, cells / 2, 0, cells - 1));

    xMapped = constrain(xMapped, 0, cells - 1);
    yMapped = constrain(yMapped, 0, cells - 1);

    return points[xMapped][yMapped];
  }

  public PVector getLerpedVector(PVector v1, PVector v2, float amt) {
    return PVector.lerp(v1, v2, amt);
  }

  public PVector getLerpedVector(int x1, int y1, int x2, int y2, float amt) {
    return PVector.lerp(getPointVector(x1, y1), getPointVector(x2, y2), amt);
  }
}
