class Layer {
  // Eye candy
  protected float progress;
  protected boolean completed;
  protected boolean dissapear;

  // Interactivity properties
  protected boolean selected;
  protected float rotation;
  protected float slideTarget;

  // Common values for ease of access
  protected int[] center;
  protected int cells;
  protected int firstCell;
  protected int lastCell;
  protected int quarterCells;
  protected int halfCells;

  protected boolean destroy;
  protected boolean shown;

  protected color deselectedColor;
  protected color selectedColor;
  protected color colour;

  public Layer(color c) {
    progress = 0;
    completed = false;
    selected = false;
    destroy = false;
    dissapear = false;
    shown = false;
    rotation = 0;

    center = g.getPoint(0, 0);
    cells = g.getCells();
    firstCell = round(-cells / 2);
    lastCell = round(cells / 2);
    quarterCells = round(cells / 8);
    halfCells = round(cells / 4);

    deselectedColor = c;
    selectedColor = color(hue(c), 80, 80);

    colour = deselectedColor;
  }

  public Layer() {
    this(color(floor(random(0, 360)), 55, 40));
  }

  public void update() {
    slide();
    
    if (progress > 0.5) {
      shown = true;
    }
  }

  public boolean getSelected() {
    return selected;
  }

  public void appear() {
    if (!completed && progress < 1) {
      progress += 0.0015 + ((1 - progress) * 0.04);
    } else if (!completed && progress >= 1) {
      completed = true;
      progress = 1;
    }
  }

  public void drop() {
    if (completed == true) {
      dissapear = true;
    }
  }

  public void select() {
    if (!selected) {
      button.trigger();
      selected = true;
      colour = selectedColor;
    }
  }

  public void deselect() {
    setSlideTarget();
    selected = false;
    colour = deselectedColor;
  }

  private void slide() {
    if (abs( - slideTarget) <= 0.01) {
      rotation = slideTarget;
    }

    if (rotation < slideTarget) {
      rotation += 0.01;
    } else if (rotation > slideTarget) {
      rotation -= 0.01;
    }
  }

  protected void setSlideTarget() {
    slideTarget = getClosestSlideTarget(rotation);
  }

  public void dissapear() {
    if (progress > 0 && dissapear == true) {
      progress = progress - (0.05 - ((progress) * 0.04));
    } else if (progress < 0 && dissapear == true) {
      destroy = true;
    }
  }

  public boolean destroy() {
    if (destroy == true) {
      return true;
    } else {
      return false;
    }
  }

  public void setProgressByTime() {

  }

  public void setRotation(float r) {
    rotation = r;
  }

  public void setRotationByMouse() {
    setRotation(atan2((height / 2) - mouseY, (width / 2) - mouseX));
    p.setLifespan();
  }

  public void addProgressByMouse() {
    float displacement = dist(mouseX, mouseY, pmouseX, pmouseY);
    float relativeDisplacement = map(displacement, 0, dist(0, 0, width, height), 0, 0.35);

    progress += (relativeDisplacement);


    if (!completed && progress > 0) {
      progress -= 0.004;
    }

    if (progress >= 1) {
      progress = 1;
      completed = true;
    }
  }

  // Override
  public void render() {
  }

  public PVector[] getTargets() {
    PVector[] dummy = new PVector[0];
    return dummy;
  }
  public Caster[] getCasters() {
    Caster[] dummy = new Caster[0];
    return dummy;
  }

  public float getProgress() {
    return progress;
  }

  public void setProgress(float p) {
    progress =p;
  }
  
  public boolean getShown() {
    return shown;
  }
}
