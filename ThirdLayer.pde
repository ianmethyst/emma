class ThirdLayer extends Layer {
  Caster[] verticalCasters;
  PVector[] verticalTargets;

  Caster[] horizontalCasters;
  PVector[] horizontalTargets; 

  public ThirdLayer(Layer lh, Layer lv) {
    super(color(0, 0, 0));
    Caster[] hC = lh.getCasters();
    Caster[] vC = lv.getCasters();

    horizontalCasters = new Caster[vC.length];
    verticalCasters = new Caster[hC.length];

    PVector[] hT = lh.getTargets();
    PVector[] vT = lv.getTargets();

    verticalTargets = new PVector[hT.length];
    horizontalTargets = new PVector[vT.length];

    for (int i = 0; i < hC.length; i++) {
      horizontalCasters[i] = new Caster(hC[i].getPoint(), 1, 3);
    }

    for (int i = 0; i < vC.length; i++) {
      verticalCasters[i] = new Caster(vC[i].getPoint(), 0, 3);
    }

    for (int i = 0; i < hT.length; i++) {
      horizontalTargets[i] = hT[i].copy();
    }

    for (int i = 0; i < vT.length; i++) {
      verticalTargets[i] = vT[i].copy();
    }

    selectedColor = color(60, 100, 100);
  }

  public void render() {
    pushMatrix();
    pushStyle();

    translate(width / 2, height / 2);
    rotate(rotation);

    stroke(colour);

    horizontalCasters[0].cast(int(horizontalTargets[0].x), int(horizontalTargets[0].y), HORIZONTAL, progress);
    horizontalCasters[0].cast(int(horizontalTargets[1].x), int(horizontalTargets[1].y), HORIZONTAL, progress);
    horizontalCasters[1].cast(int(horizontalTargets[0].x), int(horizontalTargets[0].y), HORIZONTAL, progress);
    horizontalCasters[1].cast(int(horizontalTargets[1].x), int(horizontalTargets[1].y), HORIZONTAL, progress);
    verticalCasters[0].cast(int(verticalTargets[0].x), int(verticalTargets[0].y), VERTICAL, progress);
    verticalCasters[0].cast(int(verticalTargets[1].x), int(verticalTargets[1].y), VERTICAL, progress);
    verticalCasters[1].cast(int(verticalTargets[0].x), int(verticalTargets[0].y), VERTICAL, progress);
    verticalCasters[1].cast(int(verticalTargets[1].x), int(verticalTargets[1].y), VERTICAL, progress);

    popStyle();
    popMatrix();
  }
}
