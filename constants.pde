final int HORIZONTAL = 0;
final int VERTICAL = 1;

final float LOCK_ANGLE = PI * 0.25;

final float slideTargets[] = {-PI, - PI * 0.75, -PI * 0.5, -PI * 0.25, 0, PI * 0.25, PI * 0.50, PI * 0.75, PI, PI * 1.25, PI * 1.5, PI * 1.75 * PI * 2};

final int INDIVIDUAL_LAYERS_ROTATE = 0;
final int GROW_ROTATE_ALL_LAYERS = 1;
