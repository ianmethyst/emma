class Painting {
  protected Layer layers[];
  private int selectedLayer;
  float lifespan;

  public Painting() {
    selectedLayer = -1;
    createAllLayers();
    setLifespan();
  }

  public float getLifespan() {
    return lifespan;
  }

  public void setLifespan(float l) {
    lifespan = l;
  }

  public void setLifespan() {
    setLifespan(millis() + 5 * 1000);
  }

  private void createAllLayers() {
    if (throwCoin()) {
      layers = new Layer[3];

      layers[0] = new FirstLayer(true);
      layers[1] = new CircleLayer(false);
      layers[2] = new CircleLayer(true);
    } else {
      layers = new Layer[4];

      layers[0] = new FirstLayer();
      layers[1] = new SecondLayer(HORIZONTAL);
      layers[2] = new SecondLayer(VERTICAL);
      layers[3] = new ThirdLayer(layers[1], layers[2]);
    }
  }

  public void render() {
    for (Layer l : layers) {
      l.render();
    }
  }

  public void update() {
    switch (mode) {
    case INDIVIDUAL_LAYERS_ROTATE:
      for (Layer l : layers) {
        l.update();
        setProgressByTime(l);

        if (l.destroy()) {
          createAllLayers();
          setLifespan();
        }
      }

      if (millis() > lifespan - 4 * 1000) {
        deselectAll();
      }

      selectLayerByCursor();

      if (selectedLayer != -1) {
        layers[selectedLayer].setRotation(c.getAngle());
      }
      break;

    case GROW_ROTATE_ALL_LAYERS:
      for (Layer l : layers) {
        l.update();
        setProgressByCursor(l);
      }
      break;
    }
  }

  public void dropAll() {
    for (Layer l : layers) {
      l.drop();
    }
  }

  private void selectLayerByCursor() {
    if (c.getActive()) {
      int selected = floor(c.getDist() * layers.length);
      selected = constrain(selected, 0, layers.length);
      selected = floor(map(selected, 0, layers.length, layers.length, 0));
      selectLayer(selected);
    }
  }

  private void setProgressByCursor(Layer l) {
    float d = -0.35 + c.getDist() * 2.25;
    d = constrain(d, 0, 1);

    if (l.getShown() && l.getProgress() == 0) {
      createAllLayers();
    }

    l.setProgress(d);
    l.setRotation(c.getAngle());
  }

  private void setProgressByTime(Layer l) {
    l.dissapear();
    l.appear();
  }

  private void deselectAll() {
    for (Layer layer : layers) {
      layer.deselect();
    }
  }

  public void selectLayer(int l) {
    if (l <= layers.length && l > 0) {
      selectedLayer = l - 1;

      if (!layers[selectedLayer].getSelected()) {
        deselectAll();
        layers[selectedLayer].select();
      }
    } else {
      deselectAll();
      selectedLayer = -1;
    }
  }
}
