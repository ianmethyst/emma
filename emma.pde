import ddf.minim.*;
import java.awt.Toolkit;
import java.awt.Frame;

import oscP5.*;

Minim minim;
AudioSample button;

Painting p;
Grid g;
Cursor c;

OscP5 oscP5;

PImage paper;
color bg;
boolean debugging;

final int HEIGHT = 600;
final int WIDTH = 600;

int mode;


void settings() {
  size(WIDTH, HEIGHT, P2D);
  smooth(16);
}

void setup() {
  surface.setTitle("floating");

  colorMode(HSB, 360, 100, 100);
  textAlign(RIGHT, TOP);

  mode = INDIVIDUAL_LAYERS_ROTATE;
  debugging = false;

  paper = loadImage("./paper.jpg");
  bg = color(floor(random(0, 360)), 5, 100);

  g = new Grid(WIDTH / 7);
  p = new Painting();
  c = new Cursor(true);

  minim = new Minim(this);
  button = minim.loadSample("button.wav", 2048);

  oscP5 = new OscP5(this, 12000);

  oscP5.plug(this, "leftOSC", "/leftWrist");
  oscP5.plug(this, "rightOSC", "/rightWrist");
  
}

void draw() {
  background(bg);

  if (millis() > p.getLifespan()) {
    p.dropAll();
  }

  c.update();
  g.render();
  p.update();
  p.render();

  pushStyle();
  blendMode(MULTIPLY);
  image(paper, 0, 0);
  popStyle();

  if (debugging) {
    c.debug();

    drawAxis();
    c.render();
    
    pushStyle();
    fill(0, 100, 100);
    if (mode == INDIVIDUAL_LAYERS_ROTATE) {
      text("INDIVIDUAL_LAYERS_ROTATE", width, 0);
    } else if (mode == GROW_ROTATE_ALL_LAYERS) {
      text("GROW_ROTATE_ALL_LAYERS", width, 0);
    }
    popStyle();
  }
}

void keyPressed() {
  switch (key) {
  case 'R': 
    p = new Painting();
    bg = color(floor(random(0, 360)), 5, 100);
    break;

  case 'D':
    debugging = !debugging;
    break;

  case 'd':
    p.dropAll();
    p.setLifespan(millis());
    break;

  case 'i':
    mode = INDIVIDUAL_LAYERS_ROTATE;
    p.createAllLayers();
    break;

  case 'g':
    mode = GROW_ROTATE_ALL_LAYERS;
    p.createAllLayers();
    break;

  case '1':
  case '2':
  case '3':
  case '4':
  case '0':
    if (mode == INDIVIDUAL_LAYERS_ROTATE) {
      p.selectLayer(Character.getNumericValue(key));
      p.setLifespan();
    }
    break;
  }
}

public void leftOSC(float x, float y) { 
  c.setLeft(x, y);
}

public void rightOSC(float x, float y) {
  c.setRight(x, y);
}
