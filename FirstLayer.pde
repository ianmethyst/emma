class FirstLayer extends Layer {
  // Variability properties
  private boolean fromCenterToHalf;
  private Caster caster;

  public FirstLayer(boolean fromCenterToHalf) {
    super();
    
    this.fromCenterToHalf = fromCenterToHalf;

    if (fromCenterToHalf) {
      caster = new Caster(center, halfCells + 1);
    } else {
      caster = new Caster(center, int(random(5, halfCells)));
    }
  }

  public FirstLayer() {
    this(throwCoin());
  }

  public void render() {
    pushMatrix();
    pushStyle();
    translate(width / 2, height / 2);
    rotate(rotation);

    stroke(colour);

    if (fromCenterToHalf) {
      caster.castDivided(lastCell, 0, VERTICAL, progress);
      caster.castDivided(firstCell, 0, VERTICAL, progress);
      caster.castDivided(0, firstCell, HORIZONTAL, progress);
      caster.castDivided(0, lastCell, HORIZONTAL, progress);
    } else {
      caster.cast(lastCell, 0, VERTICAL, progress);
      caster.cast(firstCell, 0, VERTICAL, progress);
      caster.cast(0, firstCell, HORIZONTAL, progress);
      caster.cast(0, lastCell, HORIZONTAL, progress);
    }

    popStyle();
    popMatrix();
  }
}
