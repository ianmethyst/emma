class CircleLayer extends Layer {
  Caster caster;

  public CircleLayer(boolean rotate) {
    super();
    caster = new Caster(center, halfCells);

    if (rotate) {
      rotation = PI * 0.5;
      setSlideTarget();
    }
  }

  public void render() {
    pushMatrix();
    pushStyle();
    translate(width / 2, height / 2);
    rotate(rotation);

    stroke(colour);

    caster.castArround(progress);

    popStyle();
    popMatrix();
  }
}
